<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/18/2018
 * Time: 10:36 PM
 */

namespace App\Middleware;

use App\Contracts\BaseMiddleware;

/**
 * Class ApiMiddleware
 *
 * @package App\Middleware
 */
class ApiMiddleware implements BaseMiddleware
{
    /**
     * @return bool
     */
    public static function check() : bool
    {
        return request()->server('HTTP_API_TOKEN') === config('common.api_token');
    }
}