<?php
/**
 * Created by PhpStorm.
 * User: Roman Murashkin
 * Date: 14.09.2018
 * Time: 17:34
 */

namespace App\Entities;

use App\Contracts\BaseEntity;

/**
 * Class ConnectEntity
 *
 * @package App\Entities
 */
class ItemEntity extends BaseEntity
{
    /** @var string  */
    protected $table = 'items';

    public $id;
    public $title;
    public $description;
    public $created_at;

}