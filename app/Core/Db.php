<?php

namespace App\Core;

use App\Contracts\BaseEntity;
use PDO;
use App\Entities\ItemEntity;
use NilPortugues\Sql\QueryBuilder\Builder\GenericBuilder;
use NilPortugues\Sql\QueryBuilder\Builder\MySqlBuilder;

/**
 * Class Db
 *
 * @package App\Core
 */
class Db
{

    /** @var PDO */
    protected $pdo;

    /** @var GenericBuilder */
    public $queryBuilder;

    /** @var string */
    protected $query;

    /**
     * Db constructor.
     */
    public function __construct()
    {
        $settings = $this->getPDOSettings();
        try {
            $this->pdo = new PDO($settings['dsn'], $settings['user'], $settings['password'], null);
            $this->queryBuilder = new MySqlBuilder();
        } catch (\Exception $exception) {
            if (config('common.debug', false)) {
                dump($exception->getMessage());
            }
        }
    }

    /**
     * @return array
     */
    protected function getPDOSettings() : array
    {
        $result = [];
        $config = config('database');
        $result['dsn'] = "{$config['type']}:host={$config['host']};dbname={$config['name']};charset={$config['charset']}";
        $result['user'] = $config['user'];
        $result['password'] = $config['password'];

        return $result;
    }

    /**
     * @param BaseEntity $entity
     * @param array      $params
     * @param bool       $first
     *
     * @return array|BaseEntity
     */
    public function execute(BaseEntity $entity, array $params = [], $first = false)
    {
        try {
            $stmt = $this->pdo->prepare($this->query);
            $stmt->execute($params);

            return $first ?
                $stmt->fetchObject(get_class($entity)) :
                $stmt->fetchAll(PDO::FETCH_CLASS, get_class($entity));
        } catch (\Exception $e) {
            dd($e->getMessage(), $e->getTraceAsString());
        }
    }

    /**
     * @return string
     */
    public function getQuery() : string
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery(string $query) : void
    {
        $this->query = $query;
    }

}