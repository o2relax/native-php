<?php

namespace App\Core;

use App\Exceptions\InvalidRouteException;
use App\Exceptions\PermissionDeniedException;
use App\Middleware\ApiMiddleware;

/**
 * Class app
 *
 * @package App\Core
 */
class App
{
    /** @var Router */
    protected static $router;

    /** @var Request */
    protected static $request;

    /** @var Config */
    protected static $configs;

    /** @var Session */
    protected static $session;

    /** @var db */
    protected static $db;

    /** @var Cache */
    protected static $cache;

    /** @var string */
    public static $token;

    public static $middleware = [
        'api' => ApiMiddleware::class,
    ];

    /**
     *
     */
    public static function init() : void
    {
        error_reporting(E_ALL);
        set_error_handler('App\Core\Error::errorHandler');
        set_exception_handler('App\Core\Error::exceptionHandler');

        spl_autoload_register(['static', 'loadClass']);
        static::bootstrap();
    }

    /**
     *
     */
    public static function bootstrap() : void
    {
        session_start();
        static::setConfigs(new Config());
        static::setSession(new Session());
        static::setRequest(new Request());
        static::setCache(new Cache());
        static::setDb(new Db());
        static::setRouter(new Router());
    }

    /**
     * @param $className
     */
    public static function loadClass($className) : void
    {
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        require_once ROOT_PATH . DIRECTORY_SEPARATOR . $className . '.php';
    }

    /**
     * @throws InvalidRouteException
     * @throws PermissionDeniedException
     */
    public static function load() : void
    {
        $router = self::getRouter();
        $router->collection($router);

        echo $router->dispatch(request()->server('REQUEST_URI'));
    }

    /**
     * @return Router
     */
    public static function getRouter() : Router
    {
        return self::$router;
    }

    /**
     * @return Request
     */
    public static function getRequest() : Request
    {
        return self::$request;
    }

    /**
     * @return db
     */
    public static function getDb() : db
    {
        return self::$db;
    }

    /**
     * @return string
     */
    public static function getToken() : string
    {
        return self::$token;
    }

    /**
     * @return Config
     */
    public static function getConfigs() : Config
    {
        return self::$configs;
    }

    /**
     * @return Session
     */
    public static function getSession() : Session
    {
        return self::$session;
    }

    /**
     * @param Router $router
     */
    public static function setRouter(Router $router) : void
    {
        self::$router = $router;
    }

    /**
     * @param Request $request
     */
    public static function setRequest(Request $request) : void
    {
        self::$request = $request;
    }

    /**
     * @param Config $configs
     */
    public static function setConfigs(Config $configs) : void
    {
        self::$configs = $configs;
    }

    /**
     * @param Session $session
     */
    public static function setSession(Session $session) : void
    {
        self::$session = $session;
    }

    /**
     * @param db $db
     */
    public static function setDb(db $db) : void
    {
        self::$db = $db;
    }

    /**
     * @param string $token
     */
    public static function setToken(string $token) : void
    {
        self::$token = $token;
    }

    /**
     * @return Cache
     */
    public static function getCache() : Cache
    {
        return self::$cache;
    }

    /**
     * @param Cache $cache
     */
    public static function setCache(Cache $cache) : void
    {
        self::$cache = $cache;
    }

    /**
     * @return array
     */
    public static function getMiddleware() : array
    {
        return self::$middleware;
    }

}