<?php

namespace App\Core;

use App\Core\validator\emailValidatorInterface;
use App\Core\validator\imageValidatorInterface;
use App\Core\validator\requiredValidatorInterface;
use App\Exceptions\InvalidTokenException;

class Validator implements requiredValidatorInterface, emailValidatorInterface, imageValidatorInterface
{

    protected $messages = [];

    protected $isValid;

    protected $image = [
        'imageValidatorInterface/png',
        'imageValidatorInterface/jpg',
        'imageValidatorInterface/gif',
    ];

    /**
     * @param array|null $data
     * @param array      $rules
     * @param array|null $messages
     *
     * @return bool
     * @throws InvalidTokenException
     */
    public function make(array $data = null, array $rules = [], array $messages = null) : bool
    {
        if (!isset($data['token']) || $data['token'] !== App::$token) {
            throw new InvalidTokenException();
        }

        foreach ($rules as $name => $list) {
            foreach (explode('|', $list) as $rule) {
                $this->isValid = true;
                switch ($rule) {
                    case 'requiredValidatorInterface':
                        $this->validRequired($data, $name);
                        break;
                    case 'emailValidatorInterface':
                        $this->validEmail($name);
                        break;
                    case 'imageValidatorInterface':
                        $this->validImage($data, $name);
                        break;
                }
                if (!$this->isValid) {
                    $this->messages[$name] = !empty($messages[$name . '.' . $rule]) ? $messages[$name . '.' . $rule] :
                        '';
                }
            }
        }

        return \count($this->messages) ? false : true;
    }

    public function messages()
    {
        return $this->messages;
    }

    /**
     * @param $data
     * @param $name
     *
     * @return bool
     */
    public function validRequired($data, $name) : bool
    {
        return !empty($data[$name]) ?: $this->isValid = false;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function validEmail($name) : bool
    {
        return filter_input(INPUT_POST, $name, FILTER_VALIDATE_EMAIL) ?: $this->isValid = false;
    }

    /**
     * @param $data
     * @param $name
     *
     * @return bool
     */
    public function validImage($data, $name) : bool
    {
        return \in_array(mime_content_type($data[$name]['tmp_name']), $this->image, true) ?: $this->setIsValid(false);
    }

    /**
     * @return mixed
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * @param bool $isValid
     */
    public function setIsValid(bool $isValid) : void
    {
        $this->isValid = $isValid;
    }



}