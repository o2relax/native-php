<?php

namespace App\Core;

/**
 * Class requests
 *
 * @package App\Core
 */
class Request
{

    protected $request = [];

    /** @var array */
    protected $get = [];
    /** @var array */
    protected $post = [];
    /** @var array */
    protected $server = [];
    /** @var array */
    protected $files = [];

    /**
     * requests constructor.
     */
    public function __construct()
    {
        $this->get = $this->parse($_GET);
        $this->post = $this->parse($_POST);
        $this->server = $this->parse($_SERVER);
        $this->files = $this->parse($_FILES);

        $_GET = null;
        $_POST = null;
        $_SERVER = null;
        $_FILES = null;
    }

    /**
     * @param array $array
     *
     * @return array
     */
    private function parse(array $array = []) : array
    {
        foreach ($array as $key => $value) {
            $array[$key] = $this->escape($value);
        }

        return $array;
    }

    /**
     * @param null $key
     * @param null $default
     *
     * @return null
     */
    public function post($key = null, $default = null)
    {
        return $this->getVariable($key, $default, 'post');
    }

    /**
     * @param null $key
     * @param null $default
     *
     * @return null
     */
    public function get($key = null, $default = null)
    {
        return $this->getVariable($key, $default);
    }

    /**
     * @param null   $key
     * @param null   $default
     * @param string $type
     *
     * @return null|string
     */
    private function getVariable($key = null, $default = null, $type = 'get')
    {
        if ($key) {
            return !empty($this->$type[$key]) ? $this->$type[$key] : $default;
        }

        return $this->$type;
    }

    /**
     * @param null $key
     *
     * @return null
     */
    public function file($key = null)
    {
        if ($key) {
            return !empty($this->files[$key]) ? $this->files[$key] : null;
        }

        return $this->files;
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public function server($key = null)
    {
        if ($key) {
            return !empty($this->server[$key]) ? $this->server[$key] : null;
        }

        return $this->server;
    }

    /**
     * @return array
     */
    public function all() : array
    {
        return array_merge(
            $this->post,
            $this->files,
            $this->get
        );
    }

    /**
     * @param $value
     *
     * @return string
     */
    private function escape($value) : string
    {
        return htmlspecialchars(stripslashes(trim(filter_var($value))));
    }

}