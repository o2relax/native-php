<?php

namespace App\Core;

use Phpfastcache\Config\ConfigurationOption;
use Phpfastcache\Helper\Psr16Adapter;

/**
 * Class Cache
 *
 * @package App\Core
 */
class Cache
{

    /** @var Psr16Adapter */
    protected $instance;

    /**
     * Cache constructor.
     */
    public function __construct()
    {
        if (!$this->instance) {
            try {
                $this->instance = new Psr16Adapter(
                    config('common.cache.driver'),
                    new ConfigurationOption([
                        'path' => STORAGE_PATH,
                    ]));
            } catch (\Exception $exception) {
                echo $exception->getMessage();
            }
        }
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        try {
            return $this->instance->get($key, $default);
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * @param     $key
     * @param     $value
     * @param int $tsl
     */
    public function set($key, $value, $tsl = 300)
    {
        try {
            $this->instance->set($key, $value, $tsl);
        } catch (\Exception $exception) {
        }
    }

}