<?php

namespace App\Core;

use Twig_Environment;
use Twig_Loader_Array;

class Pagination
{

    /** @var Twig_Environment */
    protected $twig;
    /** @var Twig_Loader_Array */
    protected $loader;

    /**
     * pagination constructor.
     */
    public function __construct()
    {
        $this->loader = new Twig_Loader_Array([
            'pagination' => file_get_contents(VIEW_PATH .
                                              DIRECTORY_SEPARATOR .
                                              config()->get('templates') .
                                              DIRECTORY_SEPARATOR .
                                              'include' .
                                              DIRECTORY_SEPARATOR .
                                              'pagination.html'),
        ]);
        $this->twig = new Twig_Environment($this->loader);
    }

    /**
     * @param $count_all
     * @param $per_page
     * @param $active_page
     * @param $get
     *
     * @return null|string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render($count_all, $per_page, $active_page, $get) : ?string
    {
        $countPages = ceil($count_all / $per_page);
        $countShowPages = 5;
        unset($get['page']);
        $url = url('/', $get);
        $urlPage = url('/', array_merge($get, ['page' => '']));
        if ($countPages > 1) {
            $prev = $active_page - 1;
            $next = $countPages - $active_page;
            if ($prev < floor($countShowPages / 2)) {
                $first = 1;
            } else {
                $first = $active_page - floor($countShowPages / 2);
            }
            $last = $first + $countShowPages - 1;
            if ($last > $countPages) {
                $first -= ($last - $countPages);
                $last = $countPages;
                if ($first < 1) {
                    $first = 1;
                }
            }
            $links = [];

            for ($i = $first; $i <= $last; $i++) {
                $links[$i] = $i;
            }

            return $this->twig->render('pagination',
                compact('active_page', 'countPages', 'prev', 'next', 'first', 'last', 'url', 'urlPage', 'links')
            );
        }

        return null;
    }

}