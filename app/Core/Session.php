<?php

namespace App\Core;

class Session
{
    /**
     * session constructor.
     */
    public function __construct()
    {
        foreach ($_SESSION as $key => $value) {
            $_SESSION[$key] = $this->escape($value);
        }
    }

    /**
     * @param null $key
     * @param null $default
     *
     * @return array|mixed|null
     */
    public function get($key = null, $default = null)
    {

        if ($key !== null) {
            $paths = explode('.', $key);
            $result = $_SESSION;
            foreach ($paths as $path) {
                if(!empty($result[$path])) {
                    $result = $result[$path];
                } else {
                    return is_null($default) ? null : $default;
                }
            }
            return $result;
        }

        return $_SESSION;
    }

    /**
     * @param null $key
     * @param null $value
     *
     * @return array|string
     */
    public function put($key = null, $value = null)
    {
        return $_SESSION[$key] = $this->escape($value);
    }

    /**
     * @param $key
     */
    public function forget($key)
    {
        unset($_SESSION[$key]);
    }

    /**
     * @return bool
     */
    public function auth()
    {
        return empty($this->get('auth')) ? false : true;
    }

    /**
     * @return array|mixed|null
     */
    public function user()
    {
        return $this->get('user');
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function isAdmin($id)
    {
        return !$this->auth() && !empty($this->get('user')['id']) && $this->get('user')['id'] === $id ? true : false;
    }

    /**
     *
     */
    public function logout()
    {
        session_destroy();
    }

    /**
     * @param $value
     *
     * @return array|string
     */
    private function escape($value)
    {
        if (is_array($value)) {
            $array = [];
            foreach ($value as $k => $v) {
                $array[$k] = $this->escape($v);
            }

            return $array;
        } else {
            return htmlspecialchars(stripslashes(trim(filter_var($value))));
        }
    }

}