<?php
/**
 * Created by PhpStorm.
 * User: Roman Murashkin
 * Date: 03.10.2018
 * Time: 10:28
 */

namespace App\core;

use App\Helpers\ResponseHelper;
use App\Transformers\ExceptionTransformer;
use League\Fractal\Resource\Item;

/**
 * Class Error
 *
 * @package App\core
 */
class Error
{

    /** @var ResponseHelper */
    protected $response;

    /**
     * @return string
     */
    public static function errorHandler() : string
    {
        $response = new ResponseHelper();
        $message = \func_get_arg(1);
        $errors = \func_get_args();

        return $response->setMessage($message)->error(\array_slice($errors, 2));
    }

    /**
     * @param \Exception $exception
     *
     * @return string
     */
    public static function exceptionHandler($exception) : string
    {
        $response = new ResponseHelper();
        $data = [];

        if (config('common.debug', false)) {
            $data['trace'] = $exception->getTraceAsString();
        }

        $data['message'] = $exception->getMessage();

        return $response->setCode($exception->getCode())
                        ->setMessage($data['message'])
                        ->error(new Item((object)$data, new ExceptionTransformer()));
    }

}