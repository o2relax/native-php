<?php
/**
 * Created by PhpStorm.
 * User: Roman Murashkin
 * Date: 03.10.2018
 * Time: 10:21
 */

namespace App\core;

use App\Contracts\BaseMiddleware;
use App\Exceptions\InvalidRouteException;
use App\Exceptions\PermissionDeniedException;

/**
 * Class RouterNew
 *
 * @package App\core
 */
class Router
{

    /** @var array */
    private $routes = [];

    /** @var array */
    private $params = [];

    /** @var array */
    private $query = [];

    /**
     * @param Router $router
     */
    public function collection(Router $router) : void
    {
        require APP_PATH . 'routes.php';
    }

    /**
     * @param       $route
     * @param array $params
     */
    public function add($route, array $params = []) : void
    {
        // Convert the route to a regular expression: escape forward slashes
        $route = preg_replace('/\//', '\\/', $route);

        // Convert variables e.g. {controller}
        $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);

        // Convert variables with custom regular expressions e.g. {id:\d+}
        $route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);

        // Add start and end delimiters, and case insensitive flag
        $route = '/^' . $route . '$/i';

        $this->routes[$route] = $params;
    }

    /**
     * @return array
     */
    public function getRoutes() : array
    {
        return $this->routes;
    }

    /**
     * @param $url
     *
     * @return bool
     * @throws PermissionDeniedException
     */
    public function match($url) : bool
    {
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                foreach ($matches as $key => $match) {
                    if (\is_string($key)) {
                        $params[$key] = $match;
                    }
                }

                $this->params = $params;

                $this->middleware();

                return true;
            }
        }

        return false;
    }

    /**
     * @throws PermissionDeniedException
     */
    private function middleware()
    {
        if (!empty($this->params['middleware'])) {

            $check = true;

            foreach ((array)$this->params['middleware'] as $middleware) {
                if (!$check || empty(App::getMiddleware()[$middleware])) {
                    continue;
                } else {
                    /** @var BaseMiddleware $class */
                    $class = App::getMiddleware()[$middleware];
                    $check && $check = $class::check();
                }
            }

            unset($this->params['middleware']);

            if(!$check) {
                throw new PermissionDeniedException();
            }
        }
    }

    /**
     * @return array
     */
    public function getParams() : array
    {
        return $this->params;
    }

    /**
     * @param $url
     *
     * @return mixed
     * @throws InvalidRouteException
     * @throws PermissionDeniedException
     */
    public function dispatch($url)
    {
        $url = $this->parseUrl($url);

        if ($this->match($url)) {
            $controller = $this->params['controller'];
            $controller = $this->convertToStudlyCaps($controller);
            $controller = $this->getNamespace() . $controller;

            if (class_exists($controller)) {
                $controllerObject = new $controller();

                $action = $this->params['action'];
                $action = $this->convertToCamelCase($action);

                if (preg_match('/action$/i', $action) === 0) {
                    $this->params[] = request();

                    return $controllerObject->$action(...\array_slice(array_values($this->params), 2));
                }
                $error = "Method $action in controller $controller cannot be called directly - remove the Action suffix to call this method";
            } else {
                $error = "Controller class $controller not found";
            }
        } else {
            $error = 'No route matched.';
        }

        if (!empty($error)) {
            throw new InvalidRouteException(env('DEBUG', false) ? $error : null);
        }
    }

    /**
     * @param $string
     *
     * @return string
     */
    protected function convertToStudlyCaps($string) : string
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    /**
     * @param $string
     *
     * @return string
     */
    protected function convertToCamelCase($string) : string
    {
        return lcfirst($this->convertToStudlyCaps($string));
    }

    /**
     * @param $url
     *
     * @return string
     */
    protected function parseUrl($url) : string
    {
        if ($url !== '') {
            $parsedUrl = parse_url($url);
            $parsedQuery = null;
            $url = (string)@$parsedUrl['path'];
            !empty($parsedUrl['query']) && $parsedQuery = $parsedUrl['query'];
            parse_str(html_entity_decode($parsedQuery), $query);

            $this->query = $query;
        }

        return trim($url, '/');
    }

    /**
     * @return string
     */
    protected function getNamespace() : string
    {
        $namespace = 'App\Controllers\\';

        if (array_key_exists('namespace', $this->params)) {
            $namespace .= $this->params['namespace'] . '\\';
        }

        return $namespace;
    }

}