<?php

namespace App\Core;

use Dotenv\Dotenv;

/**
 * Class Configs
 *
 * @package App\Core
 */
class Config
{

    /** @var array|mixed */
    protected $config = [];

    /**
     * Configs constructor.
     */
    public function __construct()
    {

        $dotEnv = new Dotenv(ROOT_PATH);
        $dotEnv->load();

        foreach (glob(CONFIG_PATH . '*.php') as $filename)
        {
            $this->config[pathinfo($filename, PATHINFO_FILENAME)] = include $filename;
        }
    }

    /**
     * @return array|mixed
     */
    public function all()
    {
        return $this->config;
    }

    /**
     * @param null $code
     * @param null $default
     *
     * @return array|mixed|null
     */
    public function get($code = null, $default = null)
    {
        if ($code !== null) {
            $paths = explode('.', $code);
            $result = $this->config;
            foreach ($paths as $path) {
                if(!empty($result[$path])) {
                    $result = $result[$path];
                } else {
                    return is_null($default) ? $code : $default;
                }
            }
            return $result;
        }

        return $this->all();
    }

}