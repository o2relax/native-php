<?php

namespace App\Core\validator;

interface RequiredValidatorInterface {

    public function validRequired($data, $name);

}