<?php

namespace App\Core\validator;

interface ImageValidatorInterface {

    public function validImage($data, $name);

}