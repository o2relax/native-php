<?php

namespace App\Core\validator;

interface EmailValidatorInterface {

    public function validEmail($name);

}