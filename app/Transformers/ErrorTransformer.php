<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 9/13/2018
 * Time: 11:25 PM
 */

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

/**
 * Class ErrorTransformer
 *
 * @package App\Transformers
 */
class ErrorTransformer extends TransformerAbstract
{

    public function transform($item) : array
    {
        return (array)$item;
    }

}