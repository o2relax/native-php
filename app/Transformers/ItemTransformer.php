<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 9/13/2018
 * Time: 11:25 PM
 */

namespace App\Transformers;

use App\Entities\ItemEntity;
use League\Fractal\TransformerAbstract;

/**
 * Class ConnectionTransformer
 *
 * @package App\Transformers
 */
class ItemTransformer extends TransformerAbstract
{

    public function transform(ItemEntity $item) : array
    {
        return [
            'id'          => (int)$item->id,
            'name'        => $item->title,
            'description' => $item->description,
            'date'        => $item->created_at,
        ];
    }

}