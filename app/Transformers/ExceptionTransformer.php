<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 9/13/2018
 * Time: 11:25 PM
 */

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

/**
 * Class ExceptionTransformer
 *
 * @package App\Transformers
 */
class ExceptionTransformer extends TransformerAbstract
{

    public function transform($item) : array
    {

        $return = [
            'message' => $item->message,
        ];

        !empty($item->trace) && $return['trace'] = $item->trace;

        return $return;
    }

}