<?php

namespace App\Contracts;

use App\Core\App;
use NilPortugues\Sql\QueryBuilder\Builder\GenericBuilder;
use NilPortugues\Sql\QueryBuilder\Manipulation\Select;

/**
 * Class BaseRepository
 *
 * @package App\Contracts
 */
abstract class BaseRepository
{

    /** @var BaseEntity */
    protected $entity;

    /**
     * BaseRepository constructor.
     *
     * @param BaseEntity $entity
     */
    public function __construct(BaseEntity $entity)
    {
        $this->setEntity($entity);
    }

    /**
     * @param int    $page
     * @param string $sort
     * @param string $order
     *
     * @return object
     */
    public function all(int $page = 1, string $sort = 'id', string $order = 'DESC')
    {
        $query = $this->query()
                      ->orderBy($sort, mb_strtoupper($order));

        return $this->execute($query);
    }

    /**
     * @param int $id
     *
     * @return BaseEntity
     */
    public function find(int $id) : BaseEntity
    {
        $query = $this->query()
                      ->where()
                      ->equals('id', $id)
                      ->end();

        return $this->execute($query, true);
    }

    /**
     * @return Select
     */
    protected function query() : Select
    {
        return $this->builder()->select()->setTable($this->getEntity()->getTable());
    }

    /**
     * @return GenericBuilder
     */
    protected function builder() : GenericBuilder
    {
        return App::getDb()->queryBuilder;
    }

    /**
     * @param      $query
     * @param bool $first
     *
     * @return array|BaseEntity
     */
    protected function execute($query, $first = false)
    {
        App::getDb()->setQuery($this->builder()->writeFormatted($query));

        return App::getDb()->execute($this->getEntity(), $this->builder()->getValues(), $first);
    }

    /**
     * @return BaseEntity
     */
    public function getEntity() : BaseEntity
    {
        return $this->entity;
    }

    /**
     * @param BaseEntity $entity
     */
    public function setEntity(BaseEntity $entity) : void
    {
        $this->entity = $entity;
    }


}
