<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/18/2018
 * Time: 10:37 PM
 */

namespace App\Contracts;

/**
 * Interface BaseMiddleware
 *
 * @package App\Contracts
 */
interface BaseMiddleware
{
    /**
     * @return bool
     */
    public static function check() : bool;
}