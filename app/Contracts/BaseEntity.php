<?php
/**
 * Created by PhpStorm.
 * User: Roman Murashkin
 * Date: 14.09.2018
 * Time: 17:29
 */

namespace App\Contracts;

/**
 * Class BaseEntity
 *
 * @package App\Contracts
 */
abstract class BaseEntity
{

    /** @var string */
    protected $table;

    /** @var int */
    public $perPage = 10;

    /**
     * @return string
     */
    public function getTable() : string
    {
        return $this->table;
    }

}