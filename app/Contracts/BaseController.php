<?php

namespace App\Contracts;

use App\Helpers\ResponseHelper;
use App\Traits\TransformTrait;
use Twig_Environment;

/**
 * Class BaseController
 *
 * @package App\Core
 */
abstract class BaseController
{

    use TransformTrait;

    /** @var Twig_Environment */
    protected $twig;

    /** @var \Twig_Loader_Filesystem */
    protected $loader;

    /** @var array */
    protected $vars = [];

    /** @var ResponseHelper */
    protected $response;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->loader = new \Twig_Loader_Filesystem(VIEW_PATH . DIRECTORY_SEPARATOR . config()->get('templates'));

        $this->twig = new Twig_Environment($this->loader, [
            'cache'       => VIEW_PATH . DIRECTORY_SEPARATOR . config()->get('templates_cache'),
            'debug'       => true,
            'auto_reload' => true,
        ]);

        $this->response = new ResponseHelper;
    }

    /**
     * @param $key
     * @param $value
     */
    protected function addVar($key, $value) : void
    {
        $this->vars[$key] = $value;
    }

    /**
     * @param       $template
     * @param array $data
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function view($template, array $data = []) : string
    {
        return $this->twig->render(str_replace('.', DIRECTORY_SEPARATOR, $template) .
                                   '.html', array_merge($data, $this->vars));
    }

}