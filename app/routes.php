<?php

/** @var \App\core\Router $router */

$router->add('', ['controller' => 'MainController', 'action' => 'index']);
$router->add('item/{id:\d+}', [
    'controller' => 'MainController',
    'action'     => 'getItem',
    'middleware' => ['api'],
]);