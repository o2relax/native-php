<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 9/15/2018
 * Time: 10:33 PM
 */

namespace App\Helpers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ResponseHelper
{

    /** @var int */
    protected $code;

    /** @var string */
    protected $message;

    /** @var array */
    protected $statuses = [
        200 => '200 OK',
        400 => '400 Bad Request',
        404 => '404 Not found',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error',
    ];

    /** @var Manager */
    protected $fractal;

    /** @var array */
    protected $data = [];

    /** @var array */
    protected $error = [];

    public function __construct()
    {
        $this->setFractal(new Manager());
        $this->setMessage($this->statuses[200]);
    }

    /**
     * @param array|Collection $data
     *
     * @return string
     */
    public function success($data = []) : string
    {
        !$this->code && $this->setCode(200);
        $this->setData($data);

        return $this->response();
    }

    /**
     * @param array|object $data
     *
     * @return string
     */
    public function error($data = []) : string
    {
        !$this->code && $this->setCode(400);
        $this->setError((array)$data);

        return $this->response();
    }

    /**
     * @return string
     */
    private function response() : string
    {
        header_remove();

        http_response_code($this->code);

        header('Cache-Control: no-transform,public,max-age=300,s-maxage=900');

        header('Content-Type: application/json');

        header('Status: ' . $this->statuses[$this->code]);

        echo json_encode(array_merge([
            'status' => $this->code < 300,
            'code' => $this->code,
            'message' => $this->message,
            'errors' => $this->error
        ], $this->data));
        die;
    }

    /**
     * @return array
     */
    public function getError() : array
    {
        return $this->error;
    }

    /**
     * @param array $error
     *
     * @return ResponseHelper
     */
    public function setError(array $error) : self
    {
        config('common.debug', false) && $this->error = $error;

        return $this;
    }

    /**
     * @param int $code
     *
     * @return ResponseHelper
     */
    public function setCode(int $code) : self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @param $message
     *
     * @return ResponseHelper
     */
    public function setMessage($message) : self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param array $statuses
     */
    public function setStatuses(array $statuses) : void
    {
        $this->statuses = $statuses;
    }

    /**
     * @param Manager $fractal
     */
    public function setFractal(Manager $fractal) : void
    {
        $this->fractal = $fractal;
    }

    /**
     * @param $data
     */
    public function setData($data) : void
    {
        if ($data instanceof Item || $data instanceof Collection) {
            $collection = $data;
        } else {
            $collection = new Collection($data);
        }

        $this->data = $this->fractal->createData($collection)->toArray();

    }

}