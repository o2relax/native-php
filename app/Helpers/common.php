<?php

if (!function_exists('dd')) {
    /**
     *
     */
    function dd()
    {
        foreach (func_get_args() as $arg) {
            dump($arg);
        }
        die;
    }
}
if (!function_exists('dump')) {
    /**
     * @param string $data
     */
    function dump($data = '')
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
}
if (!function_exists('url')) {
    /**
     * @param null       $url
     * @param array|null $array
     *
     * @return string
     */
    function url($url = null, array $array = null)
    {
        $params = null;
        if ($array) {
            foreach ($array as $k => $v) {
                $params[] = $k . '=' . $v;
            }
        }

        return 'http://' .
               $_SERVER['HTTP_HOST'] .
               ($url ? $url : '/') .
               ($params ? '?' . implode('&', $params) : '');
    }
}
if (!function_exists('redirect')) {
    /**
     * @param null $url
     *
     * @return bool
     */
    function redirect($url = null)
    {
        header('Location: ' . url($url ? $url : '/'));

        return true;
    }
}
if (!function_exists('array_only')) {
    /**
     * @param $array
     * @param $keys
     *
     * @return array
     */
    function array_only($array, $keys)
    {
        $return = [];

        foreach ($keys as $key) {
            if (isset($array[$key])) {
                $return[$key] = $array[$key];
            }
        }

        return $return;
    }
}

if (!function_exists('request')) {
    /**
     * @param null $key
     * @param null $default
     *
     * @return \App\Core\Request|null
     */
    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return \App\Core\App::getRequest();
        }

        return \App\Core\App::getRequest()->get($key, $default);
    }
}

if (!function_exists('session')) {
    /**
     * @param null $key
     * @param null $default
     *
     * @return \App\Core\Session|array|mixed|null
     */
    function session($key = null, $default = null)
    {
        if (is_null($key)) {
            return \App\Core\App::getSession();
        }

        return \App\Core\App::getSession()->get($key, $default);
    }
}

if (!function_exists('config')) {
    /**
     * @param null $key
     * @param null $default
     *
     * @return \App\Core\Config|array|mixed|null
     */
    function config($key = null, $default = null)
    {
        if (is_null($key)) {
            return \App\Core\App::getConfigs();
        }

        return \App\Core\App::getConfigs()->get($key, $default);

    }
}

if (!function_exists('cache')) {
    /**
     * @param null $key
     * @param null $default
     *
     * @return \App\Core\Cache|mixed|null
     */
    function cache($key = null, $default = null)
    {
        if (is_null($key)) {
            return \App\Core\App::getCache();
        }

        return \App\Core\App::getCache()->get($key, $default);
    }
}

if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (($valueLength = strlen($value)) > 1 && $value[0] === '"' && $value[$valueLength - 1] === '"') {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

if (! function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (!function_exists('is_assoc')) {
    /**
     * @param array $array
     *
     * @return bool
     */
    function is_assoc(array $array)
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }
}