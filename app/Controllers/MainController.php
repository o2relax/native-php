<?php

namespace App\Controllers;

use App\Contracts\BaseController;
use App\Core\Request;
use App\Entities\ItemEntity;
use App\Repositories\ItemRepository;
use App\Transformers\ItemTransformer;
use App\Transformers\ErrorTransformer;

/**
 * Class MainController
 *
 * @package App\Controllers
 */
class MainController extends BaseController
{

    /** @var ItemRepository */
    protected $repository;

    /**
     * MainController constructor.
     */
    public function __construct()
    {
        $this->repository = new ItemRepository(new ItemEntity());
        parent::__construct();
    }

    /**
     * @return string
     */
    public function index() : string
    {
        $items = $this->repository->all();

        $transformedItems = $this->transform($items, ItemTransformer::class);

        return $this->response->success($transformedItems);
    }

    /**
     * @param int     $id
     * @param Request $request
     *
     * @return string
     */
    public function getItem(int $id, Request $request) : string
    {
        $item = $this->repository->find($id);

        $transformedItem = $this->transform($item, ItemTransformer::class);

        return $this->response->success($transformedItem);
    }

}