<?php
/**
 * Created by PhpStorm.
 * User: Roman Murashkin
 * Date: 14.09.2018
 * Time: 17:27
 */

namespace App\Repositories;

use App\Contracts\BaseRepository;
use App\Entities\ItemEntity;

class ItemRepository extends BaseRepository
{

    /**
     * ItemRepository constructor.
     *
     * @param ItemEntity $entity
     */
    public function __construct(ItemEntity $entity)
    {
        parent::__construct($entity);
    }

}