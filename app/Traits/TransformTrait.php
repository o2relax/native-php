<?php

namespace App\Traits;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

/**
 * Trait ResponseTrait
 *
 * @package App\Traits
 */
trait TransformTrait
{

    /**
     * @param $data
     * @param $transformerName
     *
     * @return Collection|Item
     */
    public function transform($data, $transformerName)
    {
        if (is_assoc((array)$data)) {
            return new Item((object)$data, new $transformerName);
        }

        return new Collection((object)$data, new $transformerName);
    }

}
