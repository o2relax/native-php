<?php

namespace app;

use App\Core\App;
use App\Exceptions\InvalidRouteException;

\define('ROOT_PATH', str_replace('/', DIRECTORY_SEPARATOR, \dirname(__DIR__) . '/'));
\define('APP_PATH', str_replace('/', DIRECTORY_SEPARATOR, \dirname(__DIR__) . '/app/'));
\define('CORE_PATH', str_replace('/', DIRECTORY_SEPARATOR, \dirname(__DIR__) . '/app/Core'));
\define('CONFIG_PATH', str_replace('/', DIRECTORY_SEPARATOR, \dirname(__DIR__) . '/config/'));
\define('VIEW_PATH', str_replace('/', DIRECTORY_SEPARATOR, realpath(APP_PATH . '/View/')));
\define('STORAGE_PATH', str_replace('/', DIRECTORY_SEPARATOR, \dirname(__DIR__) . '/../storage/'));

require_once __DIR__ . '/../vendor/autoload.php';

require_once str_replace('/', DIRECTORY_SEPARATOR, CORE_PATH . '/App.php');

App::init();
try {
    App::load();
} catch (\Exception $exception) {
    throw new InvalidRouteException($exception->getMessage());
}

