<?php

namespace App\Interfaces;

interface SocialInterface {

    /**
     * SocialInterface constructor.
     */
    public function __construct();

    /**
     * @param int $limit
     * @return array
     */
    public function getLast($limit = null);

}