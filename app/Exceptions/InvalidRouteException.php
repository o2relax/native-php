<?php

namespace App\Exceptions;

use Exception;

/**
 * Class InvalidRouteException
 *
 * @package App\Exceptions
 */
class InvalidRouteException extends Exception
{

    /** @var string */
    protected $message = 'Page not found';

    /**
     * InvalidRouteException constructor.
     *
     * @param string|null $message
     */
    public function __construct(string $message = null)
    {
        parent::__construct($message ?: $this->message, 404);
    }

}