<?php

namespace App\Exceptions;

use Exception;

/**
 * Class DatabaseException
 *
 * @package App\Exceptions
 */
class DatabaseException extends Exception
{

    /** @var string */
    protected $message = 'database exception';

    /**
     * DatabaseException constructor.
     *
     * @param string|null $message
     */
    public function __construct(string $message = null)
    {
        parent::__construct($this->message, 400);
    }

}