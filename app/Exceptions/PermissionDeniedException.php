<?php

namespace App\Exceptions;

use Exception;

/**
 * Class PermissionDeniedException
 *
 * @package App\Exceptions
 */
class PermissionDeniedException extends Exception
{

    /** @var string */
    protected $message = 'Permission Denied';

    protected $code = 550;

    /**
     * InvalidRouteException constructor.
     *
     * @param string|null $message
     */
    public function __construct(string $message = null)
    {
        parent::__construct($message ?: $this->message, 550);
    }

}