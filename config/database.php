<?php

return [
    'type'     => 'mysql',
    'host'     => env('DB_HOST', 'localhost'),
    'name'     => env('DB_DATABASE', 'test'),
    'charset'  => 'utf8',
    'user'     => env('DB_USER', 'root'),
    'password' => env('DB_PASSWORD', 'secret'),
];