<?php

return [
    'app'             => 'Native PHP',
    'templates'       => 'templates',
    'templates_cache' => 'cache',
    'api_token'          => md5($_SERVER['HTTP_HOST']),
    'cache'           => [
        'driver' => env('CACHE_DRIVER'),
    ],
    'router'          => [
        'default_controller' => 'Main',
        'default_method'     => 'index',
    ],
    'debug'           => env('DEBUG', false),
];